const { Router } = require('express')
const UserService = require('../services/userService')
const {
	createUserValid,
	updateUserValid
} = require('../middlewares/user.validation.middleware')
const {
	responseMiddleware
} = require('../middlewares/response.middleware')

const router = Router()

router.get('/', (req, res) => {
	try {
		const users = UserService.getAll()
		res.send(users)
	} catch (e) {
		throw e
	}
})

router.post('/', createUserValid, (req, res) => {
	try {
		const newUser = UserService.createUser(req.body)
		res.send(newUser)
	} catch (e) {
		throw e
	}
})

router.get('/:id', (req, res) => {
	try {
		const { id } = req.params
		const user = UserService.getUserById(id)
		res.send(user)
	} catch (e) {
		throw e
	}
})

router.delete('/:id', (req, res) => {
	try {
		const { id } = req.params
		UserService.deleteUser(id)
		res.send('User was deleted.')
	} catch (e) {
		throw e
	}
})

router.put('/:id', updateUserValid, (req, res) => {
	try {
		const { id } = req.params
		const updatedUser = UserService.updateUser(id, req.body)
		res.send(updatedUser)
	} catch (e) {
		throw e
	}
})

module.exports = router
