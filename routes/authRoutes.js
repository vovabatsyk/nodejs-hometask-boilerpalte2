const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        const user = AuthService.login(req.body)
       if(user) {
           res.send(user)
       }
    } catch (err) {
        throw err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;