const { Router } = require('express')
const FighterService = require('../services/fighterService')
const {
	responseMiddleware
} = require('../middlewares/response.middleware')
const {
	createFighterValid,
	updateFighterValid
} = require('../middlewares/fighter.validation.middleware')

const router = Router()

router.get('/', (req, res) => {
	try {
		const fighters = FighterService.getAll()
		res.send(fighters)
	} catch (e) {
		throw e
	}
})

router.post('/', createFighterValid, (req, res) => {
	try {
		const newFighter = FighterService.createFighter(req.body)
		res.send(newFighter)
	} catch (e) {
		throw e
	}
})

router.get('/:id', (req, res) => {
	try {
		const { id } = req.params
		const fighter = FighterService.getFighterById(id)
		res.send(fighter)
	} catch (e) {
		throw e
	}
})

router.delete('/:id', (req, res) => {
	try {
		const { id } = req.params
		FighterService.deleteFighter(id)
		res.send('Fighter was deleted.')
	} catch (e) {
		throw e
	}
})

router.put('/:id', (req, res) => {
	try {
		const { id } = req.params
		const updatedFighter = FighterService.updateFighter(id, req.body)
		res.send(updatedFighter)
	} catch (e) {
		throw e
	}
})

module.exports = router
