const { UserRepository } = require('../repositories/userRepository')

class UserService {
	search(search) {
		const item = UserRepository.getOne(search)
		if (!item) {
			return null
		}
		return item
	}

	getAll() {
		return UserRepository.getAll()
	}

	createUser(user) {
		return UserRepository.create(user)
	}

	getUserById(id) {
		const foundUser = UserRepository.getOne(id)
		console.log('service', foundUser)

		if (!foundUser) {
			return null
		}
		return foundUser
	}

	deleteUser(id) {
		return UserRepository.delete(id)
	}

	updateUser(id, data) {
		return UserRepository.update(id, data)
	}
}

module.exports = new UserService()
