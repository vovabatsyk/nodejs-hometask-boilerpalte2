const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
	search(search) {
		const item = FighterRepository.getOne(search)
		if (!item) {
			return null
		}
		return item
	}

	getAll() {
		return FighterRepository.getAll()
	}

	createFighter(fighter) {
		return FighterRepository.create(fighter)
	}

	getFighterById(id) {
		const foundFighter = FighterRepository.getOne(id)
		if (!foundFighter) {
			return null
		}
		return foundFighter
	}

	deleteFighter(id) {
		return FighterRepository.delete(id)
	}

	updateFighter(id, data) {
		return FighterRepository.update(id, data)
	}
}

module.exports = new FighterService();