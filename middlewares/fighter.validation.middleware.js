const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    const { name, power, defense, health = 100 } = req.body
		if (!name || !power || !defense) {
			res.status(400).json({
				error: true,
				message: 'All fields are required!'
			})
			return null
		}

		if (isNaN(power) || power < 1 || power > 100) {
			res.status(400).json({
				error: true,
				message: 'Health should not be less than 1 and more than 100!'
			})
			return null
		}

		if (isNaN(defense) || defense < 1 || defense > 10) {
			res.status(400).json({
				error: true,
				message: 'Defense should not be less than 1 and more than 10!'
			})
			return null
		}

		if (isNaN(health) || health < 80 || health > 120) {
			res.status(400).json({
				error: true,
				message:
					'Health should not be less than 80 and more than 120!'
			})
			return null
		}


    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;