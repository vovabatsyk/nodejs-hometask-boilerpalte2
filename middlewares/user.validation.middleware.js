const { user } = require('../models/user')
const createUserValid = (req, res, next) => {
	const { firstName, lastName, email, phoneNumber, password } =
		req.body
	if (
		!firstName ||
		!lastName ||
		!email ||
		!phoneNumber ||
		!password
	) {
		res.status(400).json({
			error: true,
			message: 'All fields are required!'
		})
		return null
	}
	if (password.length < 3) {
		res.status(400).json({
			error: true,
			message: 'Password length must be at least 3 characters!'
		})
		return null
	}
	if ((!email, !phoneNumber, !password))
		if (!validateEmail(email)) {
			res
				.status(400)
				.json({ error: true, message: 'Email must be gmail!' })
			return null
		}
	if (!validatePhoneNumber(phoneNumber)) {
		res.status(400).json({
			error: true,
			message: 'The phone number must be + 380xxxxxxxxx'
		})
		return null
	}
	next()
}

const updateUserValid = (req, res, next) => {
	// TODO: Implement validatior for user entity during update
	next()
}

exports.createUserValid = createUserValid
exports.updateUserValid = updateUserValid

function validateEmail(email) {
	let regexEmail =
		/([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/g
	return email.match(regexEmail) ? true : false
}

function validatePhoneNumber(phoneNumber) {
	const regexPhoneNumber = /^((\+)380|0)[1-9](\d{2}){4}$/
	return phoneNumber.match(regexPhoneNumber) ? true : false
}
