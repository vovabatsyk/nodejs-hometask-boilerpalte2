const responseMiddleware = (req, res, next) => {
	switch (res.statusCode) {
		case 200:
			res.status(200).json({
				error: false,
				message: 'Success!'
			})
			break

		case 400:
			res.status(400).json({
				error: true,
				message: 'Query errors!'
			})
			break

		case 404:
			res.status(404).json({
				error: true,
				message: 'Something not found!'
			})
			break
	}

	next()
}

exports.responseMiddleware = responseMiddleware;